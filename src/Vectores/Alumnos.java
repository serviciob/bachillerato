/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vectores;

/**
 *
 * @author JHACK
 */
public class Alumnos {
    String CURP;
    String Apellido_Paterno;
    String Apellido_Materno;
    String Nombre;
    String Area;
    int Fecha_Nac_Año;
    int Fecha_Nac_Mes;
    int Fecha_Nac_Dia;
    char Sexo;
    int Edad;
    int Semestre;
    String Grupo;

    public Alumnos(String CURP, String Apellido_Paterno, String Apellido_Materno, String Nombre, String Area, int Fecha_Nac_Año, int Fecha_Nac_Mes, int Fecha_Nac_Dia, char Sexo, int Edad, int Semestre, String Grupo) {
        this.CURP = CURP;
        this.Apellido_Paterno = Apellido_Paterno;
        this.Apellido_Materno = Apellido_Materno;
        this.Nombre = Nombre;
        this.Area = Area;
        this.Fecha_Nac_Año = Fecha_Nac_Año;
        this.Fecha_Nac_Mes = Fecha_Nac_Mes;
        this.Fecha_Nac_Dia = Fecha_Nac_Dia;
        this.Sexo = Sexo;
        this.Edad = Edad;
        this.Semestre = Semestre;
        this.Grupo = Grupo;
    }

    public String getCURP() {
        return CURP;
    }

    public void setCURP(String CURP) {
        this.CURP = CURP;
    }

    public String getApellido_Paterno() {
        return Apellido_Paterno;
    }

    public void setApellido_Paterno(String Apellido_Paterno) {
        this.Apellido_Paterno = Apellido_Paterno;
    }

    public String getApellido_Materno() {
        return Apellido_Materno;
    }

    public void setApellido_Materno(String Apellido_Materno) {
        this.Apellido_Materno = Apellido_Materno;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getArea() {
        return Area;
    }

    public void setArea(String Area) {
        this.Area = Area;
    }

    public int getFecha_Nac_Año() {
        return Fecha_Nac_Año;
    }

    public void setFecha_Nac_Año(int Fecha_Nac_Año) {
        this.Fecha_Nac_Año = Fecha_Nac_Año;
    }

    public int getFecha_Nac_Mes() {
        return Fecha_Nac_Mes;
    }

    public void setFecha_Nac_Mes(int Fecha_Nac_Mes) {
        this.Fecha_Nac_Mes = Fecha_Nac_Mes;
    }

    public int getFecha_Nac_Dia() {
        return Fecha_Nac_Dia;
    }

    public void setFecha_Nac_Dia(int Fecha_Nac_Dia) {
        this.Fecha_Nac_Dia = Fecha_Nac_Dia;
    }

    public char getSexo() {
        return Sexo;
    }

    public void setSexo(char Sexo) {
        this.Sexo = Sexo;
    }

    public int getEdad() {
        return Edad;
    }

    public void setEdad(int Edad) {
        this.Edad = Edad;
    }

    public int getSemestre() {
        return Semestre;
    }

    public void setSemestre(int Semestre) {
        this.Semestre = Semestre;
    }

    public String getGrupo() {
        return Grupo;
    }

    public void setGrupo(String Grupo) {
        this.Grupo = Grupo;
    }

    
    
}
