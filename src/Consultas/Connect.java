/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Consultas;

import Vectores.Alumnos;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author JHACK
 */
public class Connect {
    public String CURP;
    public String Nombre;
    public String ApellidoP;
    public String ApellidoM;
    public String Area;
    public int Fecha_Nac_Año;
    public int Fecha_Nac_Mes;
    public int Fecha_Nac_Dia;
    public String Sexo;
    public int Edad;
    public int Semestre;
    public String Grupo;

    public Connect() {
    }
    
    public void Connect(){
        Connection c = null;
        try{
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:c:\\Bachillerato.db");
            c.close();
        }
        catch(Exception e){
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Opened Database Succesfully");
    }
    
    public void Query(Vector<Alumnos> alumnos, int Semestre){
        Connection c = null;
        Statement stmt = null;
        try{
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:c:\\Bachillerato.db");
            c.setAutoCommit(false);
            System.out.println("BD Abierta");
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM Alumnos WHERE Semestre = '" + Semestre + "'");
            while(rs.next()){
                CURP = rs.getString("CURP");
                Nombre = rs.getString("Nombre");
                ApellidoP = rs.getString("Apellido_Paterno");
                ApellidoM = rs.getString("Apellido_Materno");
                Area = rs.getString("Area");
                Fecha_Nac_Año = rs.getInt("Fecha_Nac_Año");
                Fecha_Nac_Mes = rs.getInt("Fecha_Nac_Mes");
                Fecha_Nac_Dia = rs.getInt("Fecha_Nac_Dia");
                Sexo = rs.getString("Sexo");
                Edad = rs.getInt("Edad");
                Semestre = rs.getInt("Semestre");
                Grupo = rs.getString("Grupo");
                alumnos.addElement(new Alumnos(CURP, ApellidoP, ApellidoM, Nombre, Area, Fecha_Nac_Año, Fecha_Nac_Mes, Fecha_Nac_Dia, Sexo.charAt(0), Edad, Semestre, Grupo));
                
            }
            rs.close();
            stmt.close();
            c.close();
        }
        catch(Exception e){
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Operacion Finalizada");
    }
    
    public void Select(DefaultTableModel tableModel, int Semestre){
        Connection c = null;
        Statement stmt = null;
        try{
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:c:\\Bachillerato.db");
            c.setAutoCommit(false);
            System.out.println("BD Abierta");
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM Alumnos WHERE Semestre = '" + Semestre + "'");
            Object datos[] = new Object[11];
            while(rs.next()){
                for(int i = 0; i < 11; i++){
                    datos[i] = rs.getObject(i + 1);
                }
                tableModel.addRow(datos);
                
            }
            rs.close();
            stmt.close();
            c.close();
        }
        catch(Exception e){
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Operacion Finalizada");
    }
}
