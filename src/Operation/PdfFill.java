/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operation;

import Vectores.Alumnos;
import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author Jair Gonzalez
 */
public class PdfFill {
    float subtotal = (float) 20.01;
    DecimalFormat df = new DecimalFormat("####0.00");
    public void fill(Alumnos alumno, int PerEscolarIn, int PerEscolarFin, int examenes, float total){
        try {
 	//Leer Plantilla PDF
       PdfReader reader = new PdfReader("C:\\Users\\Public\\Documents\\template.pdf");
	// Crear PDF
       PdfStamper stamp = new PdfStamper(reader, new
       			FileOutputStream("C:\\Users\\Public\\Documents\\pdf_relleno.pdf"));
 
 	// Rellenado de Campos
	AcroFields  form1 = stamp.getAcroFields();
        Calendar c1 = new GregorianCalendar();
        int dia = c1.get(Calendar.DAY_OF_MONTH);
        int mes = c1.get(Calendar.MONTH) + 1;
        int año = c1.get(Calendar.YEAR);
        form1.setField("txt_dia", ""+dia);
        form1.setField("txt_mes", String.format("%02d",mes));
        form1.setField("txt_año", ""+año);
        form1.setField("txt_plantel", "TELEBACHILLERATO EL ZACATAL");
        form1.setField("txt_localidad", "EL ZACATAL");
        form1.setField("txt_alumno", alumno.getNombre() + " " + alumno.getApellido_Paterno() + " " + alumno.getApellido_Materno());
        form1.setField("txt_clave", "30ETH0586N");
        form1.setField("txt_curp", alumno.getCURP());
        form1.setField("txt_semestre", ""+alumno.getSemestre());
        form1.setField("txt_grupo", alumno.getGrupo());
        form1.setField("txt_periodoescolar_inicio", ""+PerEscolarIn);
        form1.setField("txt_periodoescolar_final", ""+PerEscolarFin);
        switch(examenes){
            case 1:
                form1.setField("txt_primer", ""+20.01);
                subtotal = subtotal*1;
                break;
            case 2:
                form1.setField("txt_primer", ""+20.01);
                form1.setField("txt_segunda", ""+20.01);
                subtotal = subtotal*2;
                break;
            case 3:
                form1.setField("txt_primer", ""+20.01);
                form1.setField("txt_segunda", ""+20.01);
                form1.setField("txt_tercera", ""+20.01);
                subtotal = subtotal*3;
                break;
            default:
                
                break;
        }
        form1.setField("txt_subtotal", ""+subtotal);
        form1.setField("txt_iva", ""+3*examenes);
        form1.setField("txt_total1", ""+df.format(total));
        form1.setField("txt_total2", ""+0);
        form1.setField("txt_total", ""+df.format(total));
        switch(examenes){
            case 1:
                form1.setField("txt_letratotal", "VEINTITRES PUNTO CERO UNO PESOS MEXICANOS");
                break;
            case 2:
                form1.setField("txt_letratotal", "CUARENTA Y SEIS PUNTO CERO DOS PESOS MEXICANOS");
                break;
            case 3:
                form1.setField("txt_letratotal", "SESENTA Y NUEVE PUNTO CERO TRES PESOS MEXICANOS");
                break;
            default:
                break;
        }
        
        
 
	stamp.setFormFlattening(true);
	stamp.close();
 
        PrintPDF print = new PrintPDF();
        print.printPDF("C:\\Users\\Public\\Documents\\pdf_relleno.pdf");
	}catch (Exception e) {
		e.printStackTrace();
	}
    }
}
