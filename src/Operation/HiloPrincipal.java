/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operation;

import Consultas.Connect;
import Interface.Principal;
import Vectores.Alumnos;
import java.util.Vector;

/**
 *
 * @author Jair Gonzalez
 */
public class HiloPrincipal extends Thread{
    Principal p;

    public HiloPrincipal(Principal p) {
        this.p = p;
    }
    
    @Override
    public void run(){
        Connect c = new Connect();
        c.Query(p.alumnos, p.Semestre);
        p.setTexts(p.position);
        
    }
}
