/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operation;

import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;
import javax.print.PrintService;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;

/**
 *
 * @author Jair Gonzalez
 */
public class PrintPDF {
    public static PrintService choosePrinter() {
    PrinterJob printJob = PrinterJob.getPrinterJob();
    if(printJob.printDialog()) {
        return printJob.getPrintService();          
    }
    else {
        return null;
    }
}

public void printPDF(String fileName)
        throws IOException, PrinterException {

    try {
      PDDocument pdf = PDDocument.load(new File(fileName));
      PrinterJob job = PrinterJob.getPrinterJob();
      job.setPageable(new PDFPageable(pdf));
      boolean doPrint = job.printDialog();
      if(doPrint)
      job.print();
    } catch (Exception e) {
      System.out.println(e);
    }
}
}
